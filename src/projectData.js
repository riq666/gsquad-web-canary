const projects = [
  {
    title: 'Our Squad',
    image: './images/gsquadfm',
    description: 'Each friend represents a world in us a world possibly not born until they arrive it is only by this meeting that a new world is born.',
  },
];

export default projects;
